﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class CharacterController2D : MonoBehaviour
{
	[SerializeField] private float mJumpForce = 400f;
	[SerializeField] private float mCrouchSpeed = .36f;
	[Range(0, .3f)] [SerializeField] private float mMovementSmoothing = .05f;
	[SerializeField] private bool mAirControl = false;
	[SerializeField] private LayerMask mWhatIsGround;
	[SerializeField] private Transform mGroundCheck;

	const float KGroundedRadius = .2f;
	private bool _mGrounded;
	private Rigidbody2D _mRigidbody2D;
	private bool _mFacingRight = true;
	private Vector3 _mVelocity = Vector3.zero;
	

	public UnityEvent onLandEvent;

	[System.Serializable]
	public class BoolEvent : UnityEvent<bool> { }
	

	private void Awake()
	{
		_mRigidbody2D = GetComponent<Rigidbody2D>();

		if (onLandEvent == null)
			onLandEvent = new UnityEvent();
	}

	private void Update()
	{
		bool wasGrounded = _mGrounded;
		_mGrounded = false;
		
		Collider2D[] colliders = Physics2D.OverlapCircleAll(mGroundCheck.position, KGroundedRadius, mWhatIsGround);
		
		for (int i = 0; i < colliders.Length; i++)
		{
			if (colliders[i].gameObject != gameObject)
			{
				_mGrounded = true;
				if (!wasGrounded)
					onLandEvent.Invoke();
			}
		}
	}


	public void Move(float move, bool crouch, bool jump)
	{
		if (_mGrounded || mAirControl)
		{
			Vector3 targetVelocity = new Vector2(move * 10f, _mRigidbody2D.velocity.y);

			_mRigidbody2D.velocity = Vector3.SmoothDamp(_mRigidbody2D.velocity, targetVelocity, ref _mVelocity, mMovementSmoothing);


			if (move > 0 && !_mFacingRight)
			{
				Flip();
			}

			else if (move < 0 && _mFacingRight)
			{
				Flip();
			}
		}

		if (_mGrounded && jump)
		{
			_mGrounded = false;
			_mRigidbody2D.AddForce(new Vector2(0f, mJumpForce));
		}
	}


	private void Flip()
	{
		_mFacingRight = !_mFacingRight;
		
		transform.Rotate(0f, 180, 0);
	}
}
