﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject startingText;
    [SerializeField]
    private GameObject jumpingText;
    [SerializeField]
    private GameObject flyingText;
    [SerializeField]
    private GameObject shootingText;
    [SerializeField]
    private GameObject barrierText;
    [SerializeField]
    private GameObject barrelText;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("jumpingText"))
        {
            startingText.SetActive(false);
            jumpingText.SetActive(true);
        }
        
        if (other.gameObject.CompareTag("flyingText"))
        {
            jumpingText.SetActive(false);
            flyingText.SetActive(true);
        }
        
        if (other.gameObject.CompareTag("shootingText"))
        {
            flyingText.SetActive(false);
            shootingText.SetActive(true);
        }
        
        if (other.gameObject.CompareTag("barrierText"))
        {
            shootingText.SetActive(false);
            barrierText.SetActive(true);
        }
        
        if (other.gameObject.CompareTag("barrelText"))
        {
            barrierText.SetActive(false);
            barrelText.SetActive(true);
        }
    }
}
