﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class PauseMenu : MonoBehaviour
    {
        public static bool gameIsPaused = false;
        public GameObject pauseMenuUI;
        public GameObject settingsOverlay;
        public GameObject exitOverlay;
        
        void Update()
        {
            // Damit die Maus wieder zu sehen ist, wenn der Canvas aktiv ist
            if (pauseMenuUI.activeInHierarchy == true)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
            }
            //Mit Escape und P wird pause aufgerufen aber wenn gameIsPaused bereits aktiv ist 
            // wird resume aufgerufen
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
            {
                if (gameIsPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
        //Jedes Panel, welches über den Canvas gelegt wurde, wird deaktiviert,
        //der Pausecanvas wird deaktiviert, die zeit wird wieder auf 1 ("normal") gesezt
        //und die abfrage gameIsPaused ist flasch
        public void Resume()
        {
            pauseMenuUI.SetActive(false);
            Time.timeScale = 1f;
            gameIsPaused = false;
            settingsOverlay.SetActive(false);
            exitOverlay.SetActive(false);
        }

        //Der PauseCanvas wird aktiviert, die zeit auf null gesetzt und die bool abfrage wird auf true gesetzt
        void Pause()
        {
            pauseMenuUI.SetActive(true);
            Time.timeScale = 0f;
            gameIsPaused = true;
        }
        //ein weiter Panel wird aktiviert und über den pausecanvas gelegt
        public void EnableSettings()
        {
            settingsOverlay.SetActive(true);
        }

        //der weiter Panel wird deaktiviert 
        public void DisableSettings()
        {
            settingsOverlay.SetActive(false);
        }

        //ein weiter Panel wird aktiviert und über den pausecanvas gelegt
        public void EnableExit()
        {
            exitOverlay.SetActive(true);
        }

        //der weiter Panel wird deaktiviert
        public void DisableExit()
        {
            exitOverlay.SetActive(false);
        }

        //Läd die Szene Menu und setzt die zeit wieder auf 1 ("normal")
        public void LoadMenu()
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("MainMenu");
        }
        
        //Das Spiel wird beendet
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}