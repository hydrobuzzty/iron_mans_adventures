﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoATwoPR
{
    public class LevelPreview : MonoBehaviour
    {
        public GameObject previewLevel01;
        public GameObject previewLevel02;
        public GameObject previewLevel03;
        
        //Aktiviert ein gameobjekt und deaktiviert dabei zwei andere
        public void ShowLvl1()
        {
            previewLevel01.SetActive(true);
            previewLevel02.SetActive(false);
            previewLevel03.SetActive(false);
        }
        
        public void ShowLvl2()
        {
            previewLevel01.SetActive(false);
            previewLevel02.SetActive(true);
            previewLevel03.SetActive(false);
        }
        
        public void ShowLvl3()
        {
            previewLevel01.SetActive(false);
            previewLevel02.SetActive(false);
            previewLevel03.SetActive(true);
        }
        
        
    }
}