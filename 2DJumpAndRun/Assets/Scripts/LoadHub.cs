﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CoATwoPR
{
    public class LoadHub : MonoBehaviour
    {
        //Läd die Szene Levelhub
        public void LoadingHub()
        {
            SceneManager.LoadScene("LevelHub");
        }
    }
}