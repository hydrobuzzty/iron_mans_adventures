﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private int health = 100;
    public CharacterController2D controller;

    public Animator animator;

    private float horizontalMove = 0f;

    private float runSpeed = 40f;

    [SerializeField] private bool jump = false;

    private bool crouch = false;
    [SerializeField] private float jetForce;
    [SerializeField] private float jetWait;
    [SerializeField] private float jetRecovery;
    [SerializeField] private float max_fuel;
    [SerializeField] private float current_fuel;
    [SerializeField] private float current_recovery;
    [SerializeField] private bool canJet;

    [SerializeField] private bool jetDelay = false;

    private Transform ui_fuelbar;

    private Rigidbody2D _rigidbody2D;
    public GameObject jetItem;

    public GameObject shootingItem;

    public GameObject barrierItem;

    public GameObject barrier;

    public GameObject gameOverText;

    public AudioSource deathClipSource;

    public AudioClip deathClip;

    public GameObject deathImage;

    public string levelToLoad;

    public GameObject fuelBar;

    public GameObject texts;

    public GameObject player;

    public GameObject environment;

    private bool loadScene = false;

    [SerializeField] private LayerMask mWhatIsGround;

    [SerializeField] private Transform mGroundCheck;

    const float KGroundedRadius = .2f;

    private bool _mGrounded;

    public UnityEvent onLandEvent;
    

    private void JetDelay()
    {
        jetDelay = true;
    }

    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        current_fuel = max_fuel;

        ui_fuelbar = GameObject.FindWithTag("FuelBar").transform;

        deathClipSource.clip = deathClip;
    }
    
    public void DestroyBarrier()
    {
        Destroy(barrier);
    }

    public void OnLanding()
    {
        animator.SetBool("IsJumping", false);
        animator.SetBool("IsFlying", false);
        jump = false;
        jetDelay = false;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("acid"))
        {
            health = 0;
        }
    }

    void GameOverLoadScene()
    {
        SceneManager.LoadScene(levelToLoad);
    }
    private void GameOver()
    {
        deathClipSource.PlayOneShot(deathClip);        
        environment.SetActive(false);
        player.SetActive(false);
        fuelBar.SetActive(false);
        texts.SetActive(false);
        deathImage.SetActive(true);
        gameOverText.SetActive(true);
        
        Invoke(nameof(GameOverLoadScene), 5f);
    }

    private void Update()
    {
        if (health <= 0)
        {
            GameOver();
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (!shootingItem.activeSelf)
        {
            animator.SetBool("Shooting", true);
        }


        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            animator.SetBool("IsJumping", true);
            current_recovery = 0f;
            Invoke("JetDelay", 0.4f);

        }

        if (jump)
        {
            current_recovery = 0f;
        }

        if (!barrierItem.activeSelf)
        {
            barrier.SetActive(true);
            health = 100;
        }

        if (!barrierItem.activeSelf)
        {
            Invoke("DestroyBarrier", 10f);
        }
    }

    private void FixedUpdate()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);

        bool jet = Input.GetButton("Jump");

        bool wasGrounded = _mGrounded;

        _mGrounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(mGroundCheck.position, KGroundedRadius, mWhatIsGround);
        
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                _mGrounded = true;
                if (wasGrounded)
                    onLandEvent.Invoke();
            }
        }


        if (jetDelay && !_mGrounded && !jetItem.activeSelf)
        {
            canJet = true;
        }


        if (_mGrounded)
        {
            canJet = false;
        }

        if (canJet && jet && current_fuel > 0)
        {
            animator.SetBool("IsFlying", true);
            _rigidbody2D.AddForce(Vector3.up * (jetForce * Time.fixedDeltaTime), (ForceMode2D) ForceMode.Acceleration);
            current_fuel = Mathf.Max(0, current_fuel - Time.fixedDeltaTime);
        }
        else
        {
            animator.SetBool("IsFlying", false);
        }

        if (_mGrounded)
        {
            if (current_recovery < jetWait)
            {
                current_recovery = Mathf.Min(jetWait, current_recovery + Time.fixedDeltaTime);
            }
            else
            {
                current_fuel = Mathf.Min(max_fuel, current_fuel + Time.fixedDeltaTime * jetRecovery);
            }
        }

        ui_fuelbar.localScale = new Vector3(current_fuel / max_fuel, 1, 1);
    }
}

