﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace  CoATwoPR
{
    public class MainMenu : MonoBehaviour
    {
        public GameObject settingsOverlay;
        
        
        //läd die Szene Intro
        public void PlayGame()
        {
            SceneManager.LoadScene("Intro");
        }
        //ein Canvas wird aktiviert
        public void loadingSettings()
        {
            settingsOverlay.SetActive(true);
        }

        //der Canvas wird deaktiviert
        public void unloadingSettings()
        {
            settingsOverlay.SetActive(false);
        }

        //Beendet das Spiel
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}