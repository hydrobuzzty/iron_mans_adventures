﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollider : MonoBehaviour
{
    public GameObject jetItem;

    public GameObject shootItem;

    public GameObject barrierItem;

    public GameObject barrier;

    private string jetItemTag;

    private string shootItemTag;

    private string barrierItemTag;

    private bool deactivateItem = false;
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("JetItem"))
        {
            jetItem.SetActive(false);
        }

        if (collision.gameObject.CompareTag("ShootingItem"))
        {
            shootItem.SetActive(false);
        }

        if (collision.gameObject.CompareTag("BarrierItem"))
        {
            barrierItem.SetActive(false);
            barrier.SetActive(true);
        }
    }
}
